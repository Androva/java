package simulation.customfiles;

public class CustomException extends Exception {
	static final long serialVersionUID = 0;

	public CustomException(String message) {
		super(message);
	}
}