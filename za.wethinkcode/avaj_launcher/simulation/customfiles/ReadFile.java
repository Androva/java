package simulation.customfiles;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class ReadFile {
	private FileReader file;
	private BufferedReader buffer;
	public	List<String> input = new ArrayList<>();

	public ReadFile(String filename) throws CustomException {
		String line = null;

		try {
			this.file = new FileReader(filename);
			this.buffer = new BufferedReader(this.file);

			while ((line = this.buffer.readLine()) != null) {
				this.input.add(line);
			}
			this.buffer.close();
		} catch (FileNotFoundException e) {
			throw new CustomException("File '" + filename + "' does not exist.");
		} catch (IOException e) {
			throw new CustomException("Error reading file '" + filename + "'.");
		}
	}

	public void checkFileContents() throws CustomException {
		String[] inputArr = this.input.toArray(new String[this.input.size()]);
		int simRuns = 0;
		int longitude = 0;
		int latitude = 0;
		int height = 0;

		if (inputArr.length <= 0)
			throw new CustomException("File is empty.");
		if (inputArr.length < 2)
			throw new CustomException("Not aircraft found.");

		for (int i = 0; i < inputArr.length; i++) {
			if (i == 0) {
				try {
					simRuns = Integer.parseInt(inputArr[i]);
					if (simRuns <= 0) {
						throw new CustomException("Simulations to run cannot be less than 1. Error in line 1.");
					}
				} catch (NumberFormatException e) {
					throw new CustomException("Invalid integer for number of simulations to run. Error in line 1.");
				}
			} else {
				String[] line = inputArr[i].split(" ");
				if (line.length != 5)
					throw new CustomException("Invalid number of arguments. Expecting 5 but receiving " + line.length
							+ ". Error in line " + (i + 1) + ".");

				if (!line[0].toLowerCase().equals("helicopter") && !line[0].toLowerCase().equals("jetplane") && !line[0].toLowerCase().equals("baloon")) {
					throw new CustomException("Incorrect Aircraft type detected. Error in line " + (i + 1) + ".");
				} else if (!line[1].matches("\\w{1}\\d+"))
					throw new CustomException("Invalid Aircraft name. Error in line " + (i + 1) + ".");
				else {
					try {
						longitude = Integer.parseInt(line[2]);
						latitude = Integer.parseInt(line[3]);
						height = Integer.parseInt(line[4]);
					} catch (NumberFormatException e) {
						throw new CustomException(
								"No integer detected for longitude, latitude or height. Error in line " + (i + 1)
										+ ".");
					}
					if (longitude < 0 || latitude < 0 || height < 0) {
						throw new CustomException(
								"Longitude, latitude and height values cannot be negative. Error in line "
										+ (i + 1) + ".");
					}
				}
			}
		}
	}
}