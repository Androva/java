package simulation;

import simulation.weather.WeatherProvider;
import simulation.craft.Coordinates;
import simulation.customfiles.CustomException;

public class WeatherTower extends Tower {
	public	String getWeather(Coordinates coordinates) {
		return (WeatherProvider.getProvider().getCurrentWeather(coordinates));
	}

	void	changeWeather() throws CustomException {
		this.conditionsChanged();
	}
}