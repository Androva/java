package simulation.craft;

import simulation.Main;
import simulation.WeatherTower;

public class JetPlane extends Aircraft implements Flyable {
	private WeatherTower	weatherTower;
	private Coordinates		coordinates;

	JetPlane(String name, Coordinates coordinates) {
		super(name, coordinates);
		this.coordinates = coordinates;
	}
	
	public void	updateConditions() {
		int heightCheck;

		if (this.weatherTower.getWeather(this.coordinates) == "Rain") {
			this.coordinates = new Coordinates(this.coordinates.getLongitude(), this.coordinates.getLatitude() + 5, this.coordinates.getHeight());
			Main.writer.printf("JetPlane#%s(%d): The sky is slippery today.\n", this.name, this.id);
		}
		else if (this.weatherTower.getWeather(this.coordinates) == "Sun") {
			heightCheck = this.coordinates.getHeight() + 2;
			if (heightCheck > 100)
				heightCheck = 100;
			this.coordinates = new Coordinates(this.coordinates.getLongitude(), this.coordinates.getLatitude() + 10, heightCheck);
			Main.writer.printf("JetPlane#%s(%d): Much heat. Such wowe.\n", this.name, this.id);
		}			
		else if (this.weatherTower.getWeather(this.coordinates) == "Fog") {
			this.coordinates = new Coordinates(this.coordinates.getLongitude(), this.coordinates.getLatitude() + 1, this.coordinates.getHeight());
			Main.writer.printf("JetPlane#%s(%d): Grey. Very grey...\n", this.name, this.id);
		}
		else {
			this.coordinates = new Coordinates(this.coordinates.getLongitude(), this.coordinates.getLatitude(), this.coordinates.getHeight() - 7);
			Main.writer.printf("JetPlane#%s(%d): YEET.\n", this.name, this.id);
		}

		if (this.coordinates.getHeight() <= 0) {
			Main.writer.printf("JetPlane#%s(%d): Landing... Current coordinates: %d %d %d\n", this.name, this.id, this.coordinates.getLongitude(), this.coordinates.getLatitude(), this.coordinates.getHeight());
			Main.writer.printf("> Tower acknowledges JetPlane#%s(%d) unregistered from weather tower.\n", this.name, this.id);
		}
	}

	public void	registerTower(WeatherTower weatherTower) {
		this.weatherTower = weatherTower;
		this.weatherTower.register(this);
		Main.writer.printf("> Tower acknowledges JetPlane#%s(%d) registered to weather tower.\n", this.name, this.id);
	}

	public Coordinates	getCoordinates() {
		return (this.coordinates);
	}
}
