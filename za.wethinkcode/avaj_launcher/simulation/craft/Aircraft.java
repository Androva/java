package simulation.craft;

public abstract class Aircraft {
	protected long			id;
	protected String		name;
	protected Coordinates	coordinates;
	private static long		idCounter = 1;

	protected Aircraft(String name, Coordinates coordinates) {
		this.name = name;
		this.id = Aircraft.idCounter;
		this.coordinates = coordinates;
		Aircraft.idCounter++;
	}
}