package	simulation.craft;

public abstract class AircraftFactory {
	public static Flyable newAircraft(String type, String name, int longitude, int latitude, int height) {
		Coordinates coords = new Coordinates(longitude, latitude, height);

		if (type.toLowerCase().equals("jetplane"))
			return (new JetPlane(name, coords));
			
		if (type.toLowerCase().equals("baloon"))
			return (new Baloon(name, coords));

		if (type.toLowerCase().equals("helicopter"))
			return (new Helicopter(name, coords));
		
		return (null);
	}
}
