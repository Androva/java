package simulation.craft;

import simulation.Main;
import simulation.WeatherTower;

class Helicopter extends Aircraft implements Flyable {
	private WeatherTower	weatherTower;
	private Coordinates		coordinates;

	Helicopter(String name, Coordinates coordinates) {
		super(name, coordinates);
		this.coordinates = coordinates;
	}

	public void	updateConditions() {
		int heightCheck;

		if (this.weatherTower.getWeather(this.coordinates) == "Rain") {
			this.coordinates = new Coordinates(this.coordinates.getLongitude() + 5, this.coordinates.getLatitude(), this.coordinates.getHeight());
			Main.writer.printf("Helicopter#%s(%d): The rain isn't too bad, as long as it doesn't hail!\n", this.name, this.id);
		}
		else if (this.weatherTower.getWeather(this.coordinates) == "Sun") {
			heightCheck = this.coordinates.getHeight() + 2;
			if (heightCheck > 100)
				heightCheck = 100;
			this.coordinates = new Coordinates(this.coordinates.getLongitude() + 10, this.coordinates.getLatitude(), heightCheck);
			Main.writer.printf("Helicopter#%s(%d): A beautiful day - perfect for flying!\n", this.name, this.id);
		}	
		else if (this.weatherTower.getWeather(this.coordinates) == "Fog") {
			this.coordinates = new Coordinates(this.coordinates.getLongitude() + 1, this.coordinates.getLatitude(), this.coordinates.getHeight());
			Main.writer.printf("Helicopter#%s(%d): Hmmmmmmm... I can't really see much. Let's take things a bit slow...\n", this.name, this.id);
		}	
		else {
			this.coordinates = new Coordinates(this.coordinates.getLongitude(), this.coordinates.getLatitude(), this.coordinates.getHeight() - 12);
			Main.writer.printf("Helicopter#%s(%d): Ahhhh shit. Here we go again.\n", this.name, this.id);
		}
		
		if (this.coordinates.getHeight() <= 0) {
			Main.writer.printf("Helicopter#%s(%d): Landing... Current Coordinates: %d %d %d\n", this.name, this.id, this.coordinates.getLongitude(), this.coordinates.getLatitude(), this.coordinates.getHeight());
			Main.writer.printf("> Tower acknowledges Helicopter#%s(%d) unregistered from weather tower.\n", this.name, this.id);
		}
	}

	public void	registerTower(WeatherTower weatherTower) {
		this.weatherTower = weatherTower;
		this.weatherTower.register(this);
		Main.writer.printf("> Tower acknowledges Helicopter#%s(%d) registered to weather tower.\n", this.name, this.id);	
	}

	public Coordinates	getCoordinates() {
		return (this.coordinates);
	}
}
