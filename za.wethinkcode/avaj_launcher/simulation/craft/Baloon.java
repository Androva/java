package simulation.craft;

import simulation.Main;
import simulation.WeatherTower;

public class Baloon extends Aircraft implements Flyable {
	private WeatherTower	weatherTower;
	private Coordinates		coordinates;

	Baloon(String name, Coordinates coordinates) {
		super(name, coordinates);
		this.coordinates = coordinates;
	}
	
	public void	updateConditions() {
		int heightCheck;

		if (this.weatherTower.getWeather(this.coordinates) == "Rain") {
			this.coordinates = new Coordinates(this.coordinates.getLongitude(), this.coordinates.getLatitude(), this.coordinates.getHeight() - 5);
			Main.writer.printf("Baloon#%s(%d): A wet baloon is a sad baloon.\n", this.name, this.id);
		}
		else if (this.weatherTower.getWeather(this.coordinates) == "Sun") {
			heightCheck = this.coordinates.getHeight() + 4;
			if (heightCheck > 100)
				heightCheck = 100;
			this.coordinates = new Coordinates(this.coordinates.getLongitude() + 2, this.coordinates.getLatitude(), heightCheck);
			Main.writer.printf("Baloon#%s(%d): A hot baloon is a floaty baloon.\n", this.name, this.id);
		}
		else if (this.weatherTower.getWeather(this.coordinates) == "Fog") {
			this.coordinates = new Coordinates(this.coordinates.getLongitude(), this.coordinates.getLatitude(), this.coordinates.getHeight() - 3);
			Main.writer.printf("Baloon#%s(%d): A blind baloon is a confused baloon.\n", this.name, this.id);
		}
		else {
			this.coordinates = new Coordinates(this.coordinates.getLongitude(), this.coordinates.getLatitude(), this.coordinates.getHeight() - 12);
			Main.writer.printf("Baloon#%s(%d): A blind baloon is a confused baloon.\n", this.name, this.id);
		}
		if (this.coordinates.getHeight() <= 0) {
			Main.writer.printf("Baloon#%s(%d): Landing... Current coordinates: %d %d %d\n", this.name, this.id, this.coordinates.getLongitude(), this.coordinates.getLatitude(), this.coordinates.getHeight());
			Main.writer.printf("> Tower acknowledges Baloon#%s(%d) unregistered from weather tower.\n", this.name, this.id);
		}
	}

	public void	registerTower(WeatherTower weatherTower) {
		this.weatherTower = weatherTower;
		this.weatherTower.register(this);
		Main.writer.printf("> Tower acknowledges Baloon#%s(%d) registered to weather tower.\n", this.name, this.id);
	}

	public Coordinates	getCoordinates() {
		return (this.coordinates);
	}
}
