package simulation;

import simulation.craft.AircraftFactory;
import simulation.customfiles.CustomException;
import simulation.customfiles.ReadFile;
import simulation.craft.Flyable;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Main {
	private static WeatherTower		weatherTower;
	private static List<Flyable>	crafts = new ArrayList<>();
	public static PrintWriter		writer = null;

	public static void main(String[] args) {
		String[] 	line;
		int 		simulations;
		ReadFile	readFile = null;
		File		writeFile = new File("simulation.txt");

		try {
			readFile = new ReadFile(args[0]);
		}
		catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Please specify a scenario file.");
			return;
		}
		catch (CustomException e) {
			System.out.println(e.getMessage());
			return;
		}

		try {
			readFile.checkFileContents();
		} catch (CustomException e) {
			System.out.println(e.getMessage());
			return;
		}

		try {
			writer = new PrintWriter(writeFile);
		}
		catch (FileNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
			return;
		}

		simulations = Integer.parseInt(readFile.input.get(0));
		weatherTower = new WeatherTower();

		for (int i = 1; i < readFile.input.size(); i++) {
			line = readFile.input.get(i).split(" ");
			int height = Integer.parseInt(line[4]);
			
			if (height > 100)
				height = 100;
			crafts.add(AircraftFactory.newAircraft(line[0], line[1], Integer.parseInt(line[2]),
					Integer.parseInt(line[3]), height));
		}

		try {
			for (Flyable craft : crafts) {
				craft.registerTower(weatherTower);
			}
			writer.println();
		}
		catch (NullPointerException e) {
			System.out.println("Error: " + e.getMessage());
			return;
		}

		for (int j = 1; j <= simulations; j++) {
			writer.println("--- Simulation #" + j + " ---");
			try {
				weatherTower.changeWeather();
				writer.println("");
			}
			catch (CustomException e) {
				writer.println(e.getMessage());
				writer.close();
				return;
			}
		}
		writer.close();
		return;
	}
}