package simulation;

import simulation.craft.Flyable;
import simulation.customfiles.CustomException;
import java.util.ArrayList;
import java.util.List;

public abstract class Tower {
	private List<Flyable> observers = new ArrayList<Flyable>();

	public void register(Flyable flyable) {
		if (observers.contains(flyable))
			return;
		observers.add(flyable);
	}

	public void unregister(Flyable flyable) {
		if (observers.contains(flyable))
			observers.remove(flyable);
	}

	protected void conditionsChanged() throws CustomException {
		if (observers.size() <= 0)
			throw new CustomException("There are currently no airborne craft. Terminating simulation.");

		for (int i = 0; i < observers.size(); i++) {
			observers.get(i).updateConditions();
			if (observers.get(i).getCoordinates().getHeight() <= 0) {
				unregister(observers.get(i));
				i--;
			}
		}
	}
}