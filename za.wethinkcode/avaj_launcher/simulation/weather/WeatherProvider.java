package simulation.weather;

import simulation.craft.Coordinates;
import java.util.Random;

public class WeatherProvider {
	private static WeatherProvider	weatherProvider = null;
	private static String[] 		weather = new String[4];
	private Random					rand = new Random();	

	private WeatherProvider() {
		weather[0] = "Sun";
		weather[1] = "Rain";
		weather[2] = "Snow";
		weather[3] = "Fog";
	}

	public static WeatherProvider getProvider() {
		if (weatherProvider == null) {
			weatherProvider = new WeatherProvider();
		}
		return (weatherProvider);
	}

	public String getCurrentWeather(Coordinates coordinates) {
		int longitude = coordinates.getLongitude();
		int latitude = coordinates.getLatitude();
		int height = coordinates.getHeight();

		if (height <= 20 && height >= 0) {
			if (latitude <= 100 && latitude >= 0) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(2)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(3)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(1)]);
				else
					return (weather[rand.nextInt(4)]);
			} else if (latitude <= 200 && latitude > 100) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(2)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(3)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(1)]);
				else
					return (weather[rand.nextInt(1)]);
			} else if (latitude <= 300 && latitude > 200) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(3)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(2)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(1)]);
				else
					return (weather[rand.nextInt(4)]);
			} else if (latitude <= 400 && latitude > 300) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(3)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(1)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(2)]);
				else
					return (weather[rand.nextInt(3)]);
			} else
				return (weather[rand.nextInt(4)]);
		}
		
		else if (height <= 40 && height > 20) {
			if (latitude <= 100 && latitude >= 0) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(4)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(2)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(3)]);
				else
					return (weather[rand.nextInt(4)]);
			} else if (latitude <= 200 && latitude > 100) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(1)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(2)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(4)]);
				else
					return (weather[rand.nextInt(3)]);
			} else if (latitude <= 300 && latitude > 200) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(3)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(2)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(1)]);
				else
					return (weather[rand.nextInt(4)]);
			} else if (latitude <= 400 && latitude > 300) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(4)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(4)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(4)]);
				else
					return (weather[rand.nextInt(4)]);
			} else
				return (weather[rand.nextInt(4)]);
		}

		else if (height <= 60 && height < 40) {
			if (latitude <= 100 && latitude >= 0) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(1)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(2)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(4)]);
				else
					return (weather[rand.nextInt(4)]);
			} else if (latitude <= 200 && latitude > 100) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(3)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(2)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(2)]);
				else
					return (weather[rand.nextInt(1)]);
			} else if (latitude <= 300 && latitude > 200) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(1)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(4)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(2)]);
				else
					return (weather[rand.nextInt(3)]);
			} else if (latitude <= 400 && latitude > 300) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(4)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(3)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(4)]);
				else
					return (weather[rand.nextInt(3)]);
			} else
				return (weather[rand.nextInt(4)]);
		}

		else if (height <= 80 && height > 60) {
			if (latitude <= 100 && latitude >= 0) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(2)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(1)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(3)]);
				else
					return (weather[rand.nextInt(4)]);
			} else if (latitude <= 200 && latitude > 100) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(1)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(2)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(3)]);
				else
					return (weather[rand.nextInt(4)]);
			} else if (latitude <= 300 && latitude > 200) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(2)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(2)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(4)]);
				else
					return (weather[rand.nextInt(4)]);
			} else if (latitude <= 400 && latitude > 300) {
				if (longitude % 3 == 0)
					return (weather[rand.nextInt(1)]);
				else if (longitude % 4 == 0)
					return (weather[rand.nextInt(2)]);
				else if (longitude % 5 == 0)
					return (weather[rand.nextInt(3)]);
				else
					return (weather[rand.nextInt(2)]);
			} else
				return (weather[rand.nextInt(4)]);
		}
		else
			return (weather[rand.nextInt(4)]);
	}
}