package za.wethinkcode.controller;

import za.wethinkcode.model.CustomException;
import za.wethinkcode.model.HeroMenu;
import za.wethinkcode.model.ICharacter;
import za.wethinkcode.model.Map;
import za.wethinkcode.view.Gui;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

public class GUIGameEngine {
	Gui			gui;
	public HeroMenu	heroMenu;
	ICharacter	hero;
	JLabel		heroStats;
	ICharacter	enemy;
	Map			map;
	boolean		outcome;
	public boolean 	exitGui = false;

	public GUIGameEngine (HeroMenu heroMenu) {
		this.heroMenu = heroMenu;
		gui = new Gui(heroMenu, this);
		gui.StartMenu();
	}

	private boolean fight(ICharacter enemy) {
		while (hero.getHp() > 0 && enemy.getHp() > 0) {
			try {
				enemy.takeDamage(hero.attack());
				if (enemy.getHp() <= 0)
					return (true);
				hero.takeDamage(enemy.attack());
			}
			catch (CustomException e) {
				if (e.getMessage().equals("Enemy deals negative damage"))
					return (true);
				System.out.println(e.getMessage());
				return (false);
			}
		}
		if (hero.getHp() <= 0) {
			return (false);
		}
		if (enemy.getHp() <= 0) {
			return (true);
		}
		return (true);
	}

	private boolean edgeOfMap() {
		if (this.hero.getPosition().getX() == this.map.getX() || this.hero.getPosition().getX() == 0 ||
			this.hero.getPosition().getY() == this.map.getY() || this.hero.getPosition().getY() == 0)
			return (true);
		return (false);
	}

	public class Move implements ActionListener {
		boolean	enemyEncountered = false;
		JLabel	enemyStats;

		public void actionPerformed(ActionEvent e) {
			hero.printPosition();
			if (e.getActionCommand().equals("North"))
				enemyEncountered = hero.move("north", map);
			if (e.getActionCommand().equals("South"))
				enemyEncountered = hero.move("south", map);
			if (e.getActionCommand().equals("East"))
				enemyEncountered = hero.move("east", map);
			if (e.getActionCommand().equals("West"))
				enemyEncountered = hero.move("west", map);

			if (enemyEncountered == true) {
				gui.buttons.removeAll();
				gui.buttons.add(new JLabel("<html><b> ENEMY ENCOUNTERED </b></html>"));
				gui.buttons.add(heroStats);

				enemy = map.getEnemy(hero.getPosition());
				enemy.printStats();
				enemyStats = new JLabel("<html><b>" + enemy.getCharacterName() + "'s stats: </b><br/>");
				enemyStats.setText(enemyStats.getText() + "Class:\t[" + enemy.getCharacterClass() + "]<br/>");
				enemyStats.setText(enemyStats.getText() + "Lvl:\t[" + enemy.getLevel() + "]<br/>");
				enemyStats.setText(enemyStats.getText() + "HP:\t[" + enemy.getHp() + "]<br/>");
				enemyStats.setText(enemyStats.getText() + "XP:\t[" + enemy.getXp() + "]<br/>");
				enemyStats.setText(enemyStats.getText() + "Dmg:\t[" + enemy.getAd() + "]<br/>");
				enemyStats.setText(enemyStats.getText() + "Def:\t[" + enemy.getDef() + "]<br/></html>");
				gui.buttons.add(enemyStats);
				gui.buttons.revalidate();
				gui.buttons.repaint();
				outcome = fight(enemy);
			}
			if (edgeOfMap() == true) {
				System.out.println("Level passed");
				gui.frame.dispose();
				exitGui = true;
			}
		}
	}

	public boolean playMap() {
		this.hero = heroMenu.activeHero;

		map = new Map(hero);
		System.out.printf("%d x %d\n", map.getX(), map.getY());

		this.heroStats = new JLabel("<html><b>" + heroMenu.activeHero.getCharacterName() + "'s stats: </b><br/>");
		this.heroStats.setText(heroStats.getText() + "Class:\t[" + heroMenu.activeHero.getCharacterClass() + "]<br/>");
		this.heroStats.setText(heroStats.getText() + "Lvl:\t[" + heroMenu.activeHero.getLevel() + "]<br/>");
		this.heroStats.setText(heroStats.getText() + "HP:\t[" + heroMenu.activeHero.getHp() + "]<br/>");
		this.heroStats.setText(heroStats.getText() + "XP:\t[" + heroMenu.activeHero.getXp() + "]<br/>");
		this.heroStats.setText(heroStats.getText() + "Dmg:\t[" + heroMenu.activeHero.getAd() + "]<br/>");
		this.heroStats.setText(heroStats.getText() + "Def:\t[" + heroMenu.activeHero.getDef() + "]<br/></html>");

		gui.view.removeAll();

		gui.northButton.addActionListener(new Move());
		gui.southButton.addActionListener(new Move());
		gui.eastButton.addActionListener(new Move());
		gui.westButton.addActionListener(new Move());

		gui.c.gridwidth = 1;
		gui.c.gridheight = 1;
		gui.c.fill = GridBagConstraints.HORIZONTAL;
		gui.c.gridx = 1;
		gui.c.gridy = 0;
		gui.view.add(gui.northButton, gui.c);
		gui.c.gridy = 2;
		gui.view.add(gui.southButton, gui.c);
		gui.c.gridx = 0;
		gui.c.gridy = 1;
		gui.view.add(gui.eastButton, gui.c);
		gui.c.gridx = 2;
		gui.c.gridy = 1;
		gui.view.add(gui.westButton, gui.c);

		gui.view.revalidate();
		gui.view.repaint();

		return (true);
	}
}