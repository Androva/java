package za.wethinkcode.controller;

import za.wethinkcode.model.CustomException;
import za.wethinkcode.model.HeroMenu;
import za.wethinkcode.model.ICharacter;
import za.wethinkcode.model.Map;
import za.wethinkcode.view.ConsoleChooseHero;
import za.wethinkcode.view.MainMenu;

import java.util.Scanner;

public class ConsoleGameEngine {
	private Map 				map;
	private MainMenu 			mainMenu = new MainMenu();
	private HeroMenu 			heroMenu;
	private ConsoleChooseHero	chooseHero;
	private Scanner 			input = new Scanner(System.in);
	public ICharacter			hero;
	private ICharacter			enemy;
	private int					options;

	public ConsoleGameEngine(HeroMenu heroMenu) {
		this.heroMenu = heroMenu;
		try {
			if (heroMenu.activeHero.isEmpty())
				this.chooseHero = new ConsoleChooseHero(this.heroMenu);
		}
		catch (NullPointerException e) {
			this.chooseHero = new ConsoleChooseHero(this.heroMenu);
		}
	}

	public void	reset() {
		for (ICharacter hero : heroMenu.heroes) {
			hero.resetCharacter();
		}
	}

	public void characterSelection() {
		boolean flag = true;

		mainMenu.displayMainMenu();

		while (flag == true) {
			try {
				options = Integer.parseInt(input.next());
				switch (options) {
					case 1:
						try {
							chooseHero.chooseHero(input);
							flag = false;
							break;
						} catch (CustomException e) {
							System.out.println(e.getMessage());
						}
					case 2:
						try {
							chooseHero.createHero(input);
							flag = false;
							break;
						} catch (CustomException e) {
							System.out.println(e.getMessage());
						}
					default:
						System.out.println("Invalid option. Please try again.");
						break;
					}
			}
			catch (NumberFormatException e) {
				System.out.println("Invalid option. Please try again.");
			}
		}

		flag = true;
		try {
			chooseHero.characterConfirmation();
			hero = heroMenu.activeHero;
			System.out.println("\n1. Begin\n2. Choose another character");
			while (flag == true) {
				try {
					options = Integer.parseInt(input.next());
					switch (options) {
						case 1:
							flag = false;
							break;
						case 2:
							this.characterSelection();
							break;
						default:
							System.out.println("Invalid option. Please try again.");
							break;
					}
				}
				catch (NumberFormatException e) {
					System.out.println("Invalid option. Please try again.");
				}
			}
		}
		catch (CustomException e) {
			System.out.println(e.getMessage());
			return;
		}
		return;
	}

	public boolean	fight() {
		while (hero.getHp() > 0 && enemy.getHp() > 0) {
			try {
				enemy.takeDamage(hero.attack());
				if (enemy.getHp() <= 0)
					return (true);
				hero.takeDamage(enemy.attack());
			}
			catch (CustomException e) {
				if (e.getMessage().equals("Enemy deals negative damage"))
					return (true);
				System.out.println(e.getMessage());
				return (false);
			}
		}
		if (hero.getHp() <= 0) {
			return (false);
		}
		if (enemy.getHp() <= 0) {
			return (true);
		}
		return (true);
	}	

	public boolean	playMap() {
		String		direction;
		boolean		enemyEncountered = false;
		boolean		flag = true;
		boolean		flag2 = true;
		boolean		outcome = true;

		map = new Map(hero);

		hero.printStats();
		System.out.printf("TOTAL ENEMIES ON MAP: %d\n", map.occupants.size());

		while (flag == true) {
			map.printMap(hero);
			if (hero.getPosition().getX() >= map.getX() || hero.getPosition().getX() <= 0 || hero.getPosition().getY() >= map.getY() || hero.getPosition().getY() <= 0) {
				System.out.println("\n----------------------------------------------------\n CONGRATULATIONS - MAP COMPLETED.\n----------------------------------------------------\n");
				return (true);
			}
			direction = input.next();
			switch (direction) {
				case "North":
				case "N":
				case "n":
				case "north":
					enemyEncountered = hero.move("north", map);
					break;
				case "South":
				case "S":
				case "s":
				case "south":
					enemyEncountered = hero.move("south", map);
					break;
				case "East":
				case "E":
				case "e":
				case "east":
					enemyEncountered = hero.move("east", map);
					break;
				case "West":
				case "W":
				case "w":
				case "west":
					enemyEncountered = hero.move("west", map);
					break;
				default:
					System.out.println("Invalid option. Please use a valid direction i.e: 'North', 'N', 'north' or 'n'.");
					break;
			}

			if (enemyEncountered == true) {
				System.out.println("\n----------------------------------------------------\nYou have encountered an enemy!\n----------------------------------------------------\n");
				enemy = map.getEnemy(hero.getPosition());
				enemy.printStats();
				System.out.println();
				hero.printStats();
				System.out.println("You have a choice: \n\n1. Fight\n\n OR \n\n2. Flight\n\n----------------------------------------------------\n");
				flag2 = true;
				while (flag2 == true) {
					try {
						options = Integer.parseInt(input.next());
						switch (options) {
							case 1:
								System.out.printf("\n----------------------------------------------------\nYou have chosen to FIGHT %s!\n", enemy.getCharacterName());
								outcome = this.fight();
								if (outcome == true) {
									System.out.println("You win the fight!\n\n");
									if (enemy.getArtefacts().size() > 0) {
										System.out.printf("************* The enemy dropped an Artefact. Equip now? *************\n");
										enemy.getArtefacts().get(0).printArtefact();
										System.out.println("\n1. Equip\n2. Leave\n----------------------------------------------------\n");
										boolean	flag3 = true;
										while (flag3 == true) {
											try {
												options = Integer.parseInt(input.next());
												switch (options) {
													case 1:
														hero.takeArtefact(enemy.getArtefacts().get(0));
														System.out.println("\n----------------------------------------------------\nArtefact equipped! Your stats have been boosted:\n");
														hero.printStats();
														System.out.println("\n----------------------------------------------------\n");
														flag3 = false;
														break;
													case 2:
														System.out.println("\n----------------------------------------------------\nArtefact abandoned! You may continue on your quest.\n----------------------------------------------------\n");
														flag3 = false;
														break;
													default:
														System.out.println("Invalid option. Please try again.");
														break;
												}
											}
											catch (NullPointerException e) {
												System.out.println("Invalid option. Please try again.");
											}
										}
									}
									hero.levelUp(enemy);
									map.defeatedCharacter(enemy);
								}
									else {
									System.out.println("\nYou LOSE! You have now lost the level and all your XP \nand will need to start from the beginning. Ouch.\n----------------------------------------------------\n");
									hero.resetCharacter();
									return (false);
								}
								flag2 = false;
								break;
							case 2:
								System.out.printf("\n----------------------------------------------------\nYou have chosen to RUN from %s!\n", enemy.getCharacterName());
								outcome = hero.run();
								if (outcome == true)
									System.out.println("You successfully escape!\n----------------------------------------------------\n");
								else {
									System.out.println("The villain catches you during your escape! You must now FIGHT!.\n----------------------------------------------------\n");
									outcome = this.fight();
									if (outcome == true) {
										System.out.println("You win the fight!\n----------------------------------------------------\n");
										hero.levelUp(enemy);
										map.defeatedCharacter(enemy);
									}
									else {
										System.out.println("\nYou LOSE! You have now lost the level and all your XP and will need to start from the beginning. Ouch.\n----------------------------------------------------\n");
										hero.resetCharacter();
										return (false);
									}
								}
								flag2 = false;
								break;
							default:
								System.out.println("Invalid option. Please try again.");
								break;
						}
					}
					catch (NumberFormatException e) {
						System.out.println("Invalid option. Please try again.");
					}
				}
			}
		}
		return (true);
	}

	public boolean	playGame(int mapNum) {
		boolean alive;

		System.out.printf(
				"\n----------------------------------------------------\nLevel %d - Map %d\n >> Objective: Get to the outer edges of the map <<\n----------------------------------------------------\n",
				hero.getLevel(), mapNum);
		alive = this.playMap();
		return (alive);
	}
}