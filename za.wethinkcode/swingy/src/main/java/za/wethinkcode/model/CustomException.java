package za.wethinkcode.model;

public class CustomException extends Exception {
	static final long serialVersionUID = 0;

	public CustomException(String message) {
		super(message);
	}
}