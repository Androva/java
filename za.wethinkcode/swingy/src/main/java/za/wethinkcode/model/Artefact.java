package za.wethinkcode.model;

import java.util.Random;

public class Artefact {
	private String[] types = { "Helm", "Armour", "Weapon" };
	private String	type;
	private String	artefactClass;
	private int		strength;
	private String	name;
	private String[] weaponNames = { "Sword", "Staff", "Dagger", "Spear", "Longbow", "Shortbow" };
	private String[] helmNames = { "Armet", "Bascinet", "Burgonet", "Sallet" };
	private String[] armourNames = { "Chainmail", "Hauberk", "Doublet", "Breastplate" };

	public	Artefact(int level) {
		Random	rand = new Random();
		this.type = types[new Random().nextInt(3)];
		switch (this.type) {
			case "Helm":
				this.name = helmNames[new Random().nextInt(4)];
				break;
			case "Armour":
				this.name = armourNames[new Random().nextInt(4)];
				break;
			case "Weapon":
				this.name = weaponNames[new Random().nextInt(6)];
				break;
			default:
				break;
		}

		if (rand.nextInt(5) == 0) {
			this.artefactClass = "Ultimate";
			this.strength = (rand.nextInt((70 - 41) + 1) + 41) * level / 3;
		}
		else if (rand.nextInt(4) == 0) {
			this.artefactClass = "Mythic";
			this.strength = (rand.nextInt((40 - 21) + 1) + 21) * level / 3;
		}
		else if (rand.nextInt(3) == 0) {
			this.artefactClass = "Legendary";
			this.strength = (rand.nextInt((20 - 11) + 1) + 11) * level / 3;
		}
		else {
			this.artefactClass = "Epic";
			this.strength = (rand.nextInt((10 - 5) + 1) + 5) * level / 3;
		}
	}

	public Artefact(int strength, String type, String artefactClass, String name) {
		this.strength = strength;
		this.type = type;
		this.artefactClass = artefactClass;
		this.name = name;
	}

	public String getArtefactName() {
		return (this.name);
	}

	public String getType() {
		return (this.type);
	}

	public String getArtefactClass() {
		return (this.artefactClass);
	}

	public int getStrength() {
		return (this.strength);
	}

	public void printArtefact() {
		System.out.printf("%s %s:\t%d\n", this.artefactClass, this.name, this.strength);
	}
}