package za.wethinkcode.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.validation.constraints.NotNull;

public abstract class Character implements ICharacter {
	@NotNull (message = "Hero name cannot be null.")
	private String			characterName;
	@NotNull (message = "Hero class cannot be null.")
	private String			characterClass;
	private int				level = 1;
	private int				xp = 0;
	private int				ad;
	private int				def;
	private int				hp;
	private List<Artefact>	artefacts = new ArrayList<Artefact>();
	private Position		position;

	Character(String characterName, String characterClass, Position position) {
		this.characterName = characterName;
		this.characterClass = characterClass;
		this.position = position;
	}

	public void	setLevel(int level) {
		this.level = level;
	}

	public void	setXp(int xp) {
		this.xp = xp;
	}

	public void	setAd(int ad) {
		this.ad = ad;
	}

	public void	setDef(int def) {
		this.def = def;
	}

	public void	setHp(int hp) {
		this.hp = hp;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public void addArtefact(Artefact artefact) {
		this.artefacts.add(artefact);
	}

	public String	getCharacterName() {
		return (this.characterName);
	}

	public String	getCharacterClass() {
		return (this.characterClass);
	}

	public int		getLevel() {
		return (this.level);
	}

	public int		getXp() {
		return (this.xp);
	}

	public int		getAd() {
		return (this.ad);
	}

	public int		getDef() {
		return (this.def);
	}

	public int		getHp() {
		return (this.hp);
	}

	public List<Artefact>	getArtefacts() {
		return (this.artefacts);
	}

	public Artefact	getArtefact(String type) {
		if (type.equals("Armour")) {
			for (Artefact artefact : this.artefacts) {
				if (artefact.getType().equals(type))
					return (artefact);
			}
		}
		else if (type.equals("Helm")) {
			for (Artefact artefact : this.artefacts) {
				if (artefact.getType().equals(type))
					return (artefact);
			}
		}
		else if (type.equals("Weapon")) {
			for (Artefact artefact : this.artefacts) {
				if (artefact.getType().equals(type))
					return (artefact);
			}
		}
		return (null);
	}

	public Position	getPosition() {
		return (this.position);
	}

	public void		printStats() {
		System.out.printf("%s's Stats:\nClass:\t[%s]\nLvl:\t[%d]\nHP:\t[%d]\nXP:\t[%d]\nDmg:\t[%d]\nDef:\t[%d]\n", characterName, characterClass, level, hp, xp, ad, def);
		if (this.getArtefact("Weapon") != null)
			this.getArtefact("Weapon").printArtefact();
		if (this.getArtefact("Armour") != null)
			this.getArtefact("Armour").printArtefact();
		if (this.getArtefact("Helm") != null)
			this.getArtefact("Helm").printArtefact();
	}

	public String getStats(String stat) {
		if (stat.equals("name"))
			return (getCharacterName());
		if (stat.equals("class"))
			return (getCharacterClass());
		if (stat.equals("ad"))
			return (Integer.toString(getAd()));
		if (stat.equals("hp"))
			return (Integer.toString(getHp()));
		if (stat.equals("def"))
			return (Integer.toString(getDef()));
		if (stat.equals("level"))
			return (Integer.toString(getLevel()));
		return (null);
	}

	public void		printPosition() {
		System.out.printf("[%d:%d]\n", this.position.getX(), this.position.getY());
	}

	public boolean	move(String direction, Map map) {
		if (direction.equals("north"))
			this.position = new Position(this.getPosition().getX(), this.getPosition().getY() - 1);
		if (direction.equals("south"))
			this.position = new Position(this.getPosition().getX(), this.getPosition().getY() + 1);
		if (direction.equals("east"))
			this.position = new Position(this.getPosition().getX() + 1, this.getPosition().getY());
		if (direction.equals("west"))
			this.position = new Position(this.getPosition().getX() - 1, this.getPosition().getY());

		for (Position position : map.occupied) {
			if (position.posEqual(this.position))
				return (true);
		}
		return (false);
	}

	public int	attack() {
		if (this.artefacts.size() > 0) {
			try {
				int	totalAd = this.ad + this.getArtefact("Weapon").getStrength();
				System.out.printf("%s attacks for %d damage.\n", this.characterName, totalAd);
				return (totalAd);
			}
			catch (NullPointerException e) {
				System.out.printf("%s attacks for %d damage.\n", this.characterName, this.ad);
				return (this.ad);
			}
		}
		System.out.printf("%s attacks for %d damage.\n", this.characterName, this.ad);
		return (this.ad);
	}

	public int	takeDamage(int damage) throws CustomException {
		int	totalDmg = 0;
		if (this.artefacts.size() > 0) {
			try {
				totalDmg = damage - this.def + this.getArtefact("Armour").getStrength();
				if (totalDmg > 0) {
					this.hp -= totalDmg;
					System.out.printf("%s takes %d damage. HP = %d\n", this.characterName, totalDmg, this.hp);
					return (totalDmg);
				}
				else if (this.getCharacterClass().equals("Mage") || this.getCharacterClass().equals("Tank") || this.getCharacterClass().equals("Fighter"))
					throw new CustomException("Enemy deals negative damage");
				else
					throw new CustomException("Hero deals negative damage");
			}
			catch (NullPointerException e) {
				totalDmg = damage - this.def;
				if (totalDmg > 0) {
					this.hp -= (totalDmg);
					System.out.printf("%s takes %d damage. HP = %d\n", this.characterName, totalDmg, this.hp);
					return (totalDmg);
				}
				else if (this.getCharacterClass().equals("Mage") || this.getCharacterClass().equals("Tank") || this.getCharacterClass().equals("Fighter"))
					throw new CustomException("Enemy deals negative damage");
				else
					throw new CustomException("Hero deals negative damage");
			}
		}
		else {
			totalDmg = damage - this.def;
			if (totalDmg > 0) {
				this.hp -= (totalDmg);
				System.out.printf("%s takes %d damage. HP = %d\n", this.characterName, totalDmg, this.hp);
				return (totalDmg);
			}
			else if (this.getCharacterClass().equals("Mage") || this.getCharacterClass().equals("Tank") || this.getCharacterClass().equals("Fighter"))
				throw new CustomException("Enemy deals negative damage");
			else
				throw new CustomException("Hero deals negative damage");
		}
	}

	public boolean	run() {
		Random	rand = new Random();
		if (rand.nextInt(2) == 1)
			return (true);
		return (false);
	}

	public void		takeArtefact(Artefact artefact) {
		try {
			File file = new File("./heroes.txt"); 
			BufferedReader br = new BufferedReader(new FileReader(file));
			String str;
			List<String> fileContents = new ArrayList<String>();

			while ((str = br.readLine()) != null) {
				String[] arr = str.split("\t");
				if (arr[0].equals(this.getCharacterName()) && arr[1].equals(this.getCharacterClass())) {
					String[] art = arr[3].split(",");

					if (artefact.getType() == "Helm") {
						art[0] = "[" + artefact.getArtefactClass() + " " + artefact.getArtefactName() + " " + artefact.getStrength();
					}
					else if (artefact.getType() == "Armour") {
						art[1] = artefact.getArtefactClass() + " " + artefact.getArtefactName() + " " + artefact.getStrength();
					}
					else if (artefact.getType() == "Weapon") {
						art[2] = artefact.getArtefactClass() + " " + artefact.getArtefactName() + " " + artefact.getStrength() + "]";
					}
					StringBuilder builder = new StringBuilder();
					for (int i = 0; i < art.length; i++) {
						builder.append(art[i]);
						if (i != art.length - 1)
							builder.append(",");
					}
					arr[3] = builder.toString();
				}

				StringBuilder builder = new StringBuilder();
				for (String value : arr) {
					builder.append(value + "\t");
				}
				String text = builder.toString();
				fileContents.add(text);
			}
			
			file.delete();
			FileWriter fileWriter = new FileWriter("./heroes.txt", true);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			for (String s : fileContents) {
				printWriter.println(s);
			}
			printWriter.close();
			br.close();
		}
		catch (IOException e) {
			System.out.printf("ERROR: %s\n", e.getMessage());
			return;
		}
		Artefact current = null;
		if (artefact.getType().equals("Weapon")) {
			try {
				current = this.getArtefact("Weapon");
				this.ad += artefact.getStrength();
				artefacts.remove(current);
			}
			catch (NullPointerException e) {
				;
			}
		}
		else if (artefact.getType().equals("Armour")) {
			try {
				current = this.getArtefact("Armour");
				this.def += artefact.getStrength();
				artefacts.remove(current);
			}
			catch (NullPointerException e) {
				;
			}
		}
		else if (artefact.getType().equals("Helm")) {
			try {
				current = this.getArtefact("Helm");
				this.hp += artefact.getStrength();
				artefacts.remove(current);
			}
			catch (NullPointerException e) {
				;
			}
		}
		artefacts.add(artefact);
	}

	public void		levelUp(ICharacter enemy) {
		int	levelUp	= this.level * 1000 + ((this.level - 1) * (this.level - 1)) * 450;
		this.xp += (100 * (enemy.getXp() / (this.level + 1))) / enemy.getLevel();
		
		if (this.xp >= levelUp) {
			this.level++;
			System.out.printf("\n************* %s has reached level %d! *************\n", this.characterName, this.level);

			try {
				File file = new File("./heroes.txt"); 
				BufferedReader br = new BufferedReader(new FileReader(file));
				String str;
				List<String> fileContents = new ArrayList<String>();
				String newS;

				while ((str = br.readLine()) != null) {
					newS = "";
					String[] arr = str.split("\t");
					if (arr[0].equals(this.getCharacterName()) && arr[1].equals(this.getCharacterClass())) {
						arr[2] = Integer.toString(this.level);
					}

					for (String s : arr) {
						newS += s + "\t";
					}
					fileContents.add(newS);
				}
				
				file.delete();
				FileWriter fileWriter = new FileWriter("./heroes.txt", true);
				PrintWriter printWriter = new PrintWriter(fileWriter);
				for (String s : fileContents) {
					printWriter.println(s);
				}
				printWriter.close();
				br.close();
			}
			catch (IOException e) {
				System.out.printf("ERROR: %s\n", e.getMessage());
				return;
			}
		}	
	}

	public void		resetCharacter() {
		if (this.getCharacterClass().equals("Mage")) {
			this.hp = 80;
			this.ad = 7;
			this.def = 5;
		}
		else if (this.getCharacterClass().equals("Tank")) {
			this.hp = 100;
			this.ad = 4;
			this.def = 8;
		}
		else if (this.getCharacterClass().equals("Fighter")) {
			this.hp = 75;
			this.ad = 9;
			this.def = 3;
		}
		this.xp = 0;
		this.level = 0;
		if (artefacts.size() > 0) {
			for (Iterator<Artefact> iterator = artefacts.iterator(); iterator.hasNext();) {
				iterator.next();
				iterator.remove();
			}
		}
	}

	public boolean	isEmpty() {
		if (this.characterClass == null && this.characterName == null)
			return (true);
		return (false); 
	}
}