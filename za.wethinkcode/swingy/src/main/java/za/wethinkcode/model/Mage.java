package za.wethinkcode.model;

public class Mage extends Character {
	private int			hp = 80;
	private int 		ad = 8;
	private int 		def = 5;
	private Position	position;
	
	Mage(String name, Position position, int level) {
		super(name, "Mage", position);
		super.setHp(hp);
		super.setAd(ad);
		super.setDef(def);
		if (level != 0)
			super.setXp(level * 1000 + ((level - 1) * (level - 1)) * 450);
		else
			super.setXp(0);
		super.setLevel(level);
		this.position = position;
	}

}