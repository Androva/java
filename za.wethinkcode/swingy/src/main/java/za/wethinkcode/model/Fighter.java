package za.wethinkcode.model;

public class Fighter extends Character {
	private int			hp = 75;
	private int 		ad = 10;
	private int 		def = 3;
	private Position	position;
	
	Fighter(String name, Position position, int level) {
		super(name, "Fighter", position);
		super.setHp(hp);
		super.setAd(ad);
		super.setDef(def);
		if (level != 0)
			super.setXp(level * 1000 + ((level - 1) * (level - 1)) * 450);
		else
			super.setXp(0);
		super.setLevel(level);
		this.position = position;
	}
}