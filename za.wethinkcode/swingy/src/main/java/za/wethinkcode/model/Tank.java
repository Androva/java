package za.wethinkcode.model;

public class Tank extends Character {
	private int			hp = 100;
	private int 		ad = 7;
	private int 		def = 8;
	private Position	position;
	
	Tank(String name, Position position, int level) {
		super(name, "Tank", position);
		super.setHp(hp);
		super.setAd(ad);
		super.setDef(def);
		if (level != 0)
			super.setXp(level * 1000 + ((level - 1) * (level - 1)) * 450);
		else
			super.setXp(0);
		super.setLevel(level);
		this.position = position;
	}
}