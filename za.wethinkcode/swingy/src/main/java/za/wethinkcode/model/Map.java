package za.wethinkcode.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Map {
	public List<ICharacter>		occupants = new ArrayList<ICharacter>();
	private int[]				size = new int[2];
	public List<Position>		occupied = new ArrayList<Position>();
	private Random				rand = new Random();

	public Map(ICharacter hero) {
		this.size[0] = (hero.getLevel() - 1) * 5 + 10 - (hero.getLevel() % 2);
		this.size[1] = (hero.getLevel() - 1) * 5 + 10 - (hero.getLevel() % 2);

		hero.setPosition(new Position(size[0] / 2, size[1] / 2));

		if (hero.getCharacterClass().equals("Fighter"))
			hero.setHp(75);
		else if (hero.getCharacterClass().equals("Mage"))
			hero.setHp(80);
		else if (hero.getCharacterClass().equals("Tank"))
			hero.setHp(100);

		for (int i = 1; i < size[0] + ((hero.getLevel() + 1 * rand.nextInt(5) - 1) + 1) + 1; i++) {
			occupants.add(CharacterFactory.newCharacter("Enemy" + i, "Enemy", hero, hero.getLevel(), occupied));
			occupied.add(occupants.get(i - 1).getPosition());
			occupants.get(i - 1).setXp((hero.getXp() - hero.getLevel() * rand.nextInt(4)) + 10 * hero.getLevel());
		}
	}

	public int	getX() {
		return (this.size[0]);
	}

	public int	getY() {
		return (this.size[1]);
	}

	public ICharacter	getEnemy(Position position) {
		for (ICharacter occupant : occupants) {
			if (occupant.getPosition().posEqual(position))
				return (occupant);
		}
		return (null);
	}

	public void	defeatedCharacter(ICharacter character) {
		if (occupants.contains(character)) {
			occupied.remove(character.getPosition());
			occupants.remove(character);
		}		
	}

	public void printMap(ICharacter hero) {
		String ANSI_RESET = "\u001B[0m";
		String ANSI_CYAN = "\u001B[36m";
		String ANSI_PURPLE = "\u001B[35m";

		for (int i = 0; i < size[0]; i++) {
			for (int j = 0; j < size[1]; j++) {
				if (j == hero.getPosition().getX() && i == hero.getPosition().getY()) {
					System.out.print(ANSI_PURPLE + "x " + ANSI_RESET);
				}
				else
					System.out.print(ANSI_CYAN + ". " + ANSI_RESET);
			}
			System.out.println();
		}
	}
}