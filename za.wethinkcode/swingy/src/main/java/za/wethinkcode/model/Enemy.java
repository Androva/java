package za.wethinkcode.model;

import java.util.List;
import java.util.Random;

public class Enemy extends Character {
	private int			hp;
	private int 		ad;
	private int 		def;
	private Position	heroSpawn;
	private Random		rand = new Random();

	Enemy(String name, List<Position> occupied, ICharacter hero, String eClass, int mapSize, int x, int y, int mapCentre) {
		super(name, eClass, new Position(x, y));
		this.hp = hero.getHp() - (10 * rand.nextInt((3 - 1) + 1) + 1);
		this.ad = hero.getAd() - (hero.getLevel() * 2);
		this.def = hero.getDef() - (hero.getLevel() * 2);

		heroSpawn = new Position(mapCentre, mapCentre);

		if (this.getCharacterClass().equals("Orc"))
			this.hp += rand.nextInt(2 * hero.getLevel() + 1);
		else if (this.getCharacterClass().equals("Spider"))
			this.ad += rand.nextInt(2 * hero.getLevel() + 1);
		else if (this.getCharacterClass().equals("Goblin"))
			this.def += rand.nextInt(2 * hero.getLevel() + 1);
		for (int i = 0; i < occupied.size(); i++) {
			if (occupied.get(i).posEqual(this.getPosition()) || heroSpawn.posEqual(this.getPosition())) {
				this.setPosition(new Position(rand.nextInt(mapSize), rand.nextInt(mapSize)));
			}
		}
		
		if (rand.nextInt(5) == 0) {
			this.addArtefact(new Artefact(this.getLevel()));

			if (this.getArtefact("Weapon") != null)
				this.setAd(this.getAd() + this.getArtefact("Weapon").getStrength());
			if (this.getArtefact("Armour") != null)
				this.setDef(this.getDef() + this.getArtefact("Armour").getStrength());
			if (this.getArtefact("Helm") != null)
				this.setHp(this.getHp() + this.getArtefact("Helm").getStrength());
		}

		if (this.hp <= 0)
			this.hp = 10 * hero.getLevel();
		if (this.ad <= 0)
			this.hp = 2 * hero.getLevel();
		if (this.def <= 0)
			this.def = 1 * hero.getLevel();

		super.setHp(this.hp);
		super.setAd(this.ad);
		super.setDef(this.def);
	}
}