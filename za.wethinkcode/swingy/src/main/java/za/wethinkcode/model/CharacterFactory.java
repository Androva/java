package za.wethinkcode.model;

import java.util.List;
import java.util.Random;

public abstract class CharacterFactory {
	public static ICharacter newCharacter(String name, String characterClass, ICharacter hero, int level, List<Position> occupied) {
		int			mapSize = (level - 1) * 5 + 10 - (level % 2);
		ICharacter	character;
		String[]	eClass = { "Goblin", "Orc", "Spider" };
		Random		rand = new Random();

		// try {
		// ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		// Validator validator = factory.getValidator();

		if (characterClass.equals("Tank"))
			character = new Tank(name, null, level);
		else if (characterClass.equals("Mage"))
			character = new Mage(name, null, level);
		else if (characterClass.equals("Fighter"))
			character = new Fighter(name, null, level);
		else if (characterClass.equals("Enemy"))
			character = new Enemy(name, occupied, hero, eClass[rand.nextInt(3)], mapSize, rand.nextInt(mapSize), rand.nextInt(mapSize), hero.getPosition().getX());
		else
			character = null;

		// Set<ConstraintViolation<Character>> violations =
		// validator.validate(character);

		// for (ConstraintViolation<Character> violation : violations) {
		// System.out.println(violation.getMessage());
		// return (null);
		// }
		return (character);
		// }
		// catch (NoClassDefFoundError e) {
		// System.out.println(e.getLocalizedMessage());
		// return (null);
		// }
	}
}