package za.wethinkcode.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import za.wethinkcode.view.Gui.newHero;

public class HeroMenu {
	public List<ICharacter> heroes = new ArrayList<ICharacter>();
	public ICharacter activeHero;

	public HeroMenu() {
		File file = new File("./heroes.txt"); 
	
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));

			String str;
			while ((str = br.readLine()) != null) {
				String[] arr = str.split("\t");
				ICharacter newHero = CharacterFactory.newCharacter(arr[0], arr[1], null, Integer.parseInt(arr[2]), null);
				String[] artefacts = arr[3].split(",");
				for (int i = 0; i < artefacts.length; i++) {
					String[] split = artefacts[i].split(" ");
					String type = "";
					if (split.length == 3) {
						int strength = Integer.parseInt(split[2].replace("]", ""));
						if (i == 0) {
							type = "Helm";
							newHero.setHp(newHero.getHp() + strength);
						}
						else if (i == 1) {
							type = "Armour";
							newHero.setDef(newHero.getDef() + strength);
						}
						else {
							type = "Weapon";
							newHero.setAd(newHero.getAd() + strength);
						}
						Artefact newArtefact = new Artefact(strength, type, split[0].replace("[", ""), split[1]);
						newHero.addArtefact(newArtefact);
					}
				}
				heroes.add(newHero);
			}
			br.close();
		}
		catch (IOException e) {
			return;
		}
	}
}