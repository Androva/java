package za.wethinkcode.model;

import java.util.List;

public interface ICharacter {
	void			setLevel(int level);
	void			setXp(int xp);
	void			setAd(int ad);
	void			setDef(int def);
	void			setHp(int hp);
	void			setPosition(Position position);
	void			addArtefact(Artefact artefact);
	String			getCharacterName();
	String			getCharacterClass();
	int				getLevel();
	int				getXp();
	int				getAd();
	int				getDef();
	int				getHp();
	List<Artefact>	getArtefacts();
	Artefact		getArtefact(String type);
	Position		getPosition();
	void			printStats();
	String			getStats(String stat);
	void			printPosition();
	boolean			move(String direction, Map map);
	int				attack();
	int				takeDamage(int damage) throws CustomException;
	boolean			run();
	void			takeArtefact(Artefact artefact);
	void			levelUp(ICharacter enemy);
	void			resetCharacter();
	boolean			isEmpty();
}