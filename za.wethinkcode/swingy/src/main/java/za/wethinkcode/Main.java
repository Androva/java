package za.wethinkcode;

import za.wethinkcode.controller.ConsoleGameEngine;
import za.wethinkcode.controller.GUIGameEngine;
import za.wethinkcode.model.HeroMenu;

public class Main {
	static ConsoleGameEngine	cEngine;
	static boolean				alive = true;
	static GUIGameEngine		gEngine;
	static HeroMenu				heroMenu = new HeroMenu();

	public static void main(String[] args) {
		int 	i = 1;
		String	engine ;
		boolean	flag = true;
		boolean	gameStart = true;

		try {
			engine = args[0].toLowerCase();	
		}
		catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Please select whether you'd like to launch the game in the console or gui.");
			return;
		}
		while (true) {
			while (flag == true) {
				switch (engine) {
					case "console":
						cEngine = new ConsoleGameEngine(heroMenu);
						if (gameStart == false)
							cEngine.reset();
						gameStart = false;
						cEngine.characterSelection();
						flag = false;
						break;
					case "gui":
						gEngine = new GUIGameEngine(heroMenu);
						flag = false;
						break;
					default:
						System.out.println("Invalid option. Please try again.");
						break;
				}
			}

			alive = true;
			while (alive == true) {
				System.out.println();
				if (gEngine != null && gEngine.exitGui == true) {
					System.out.println("Changing to console...");
					cEngine = new ConsoleGameEngine(gEngine.heroMenu);
					cEngine.hero = gEngine.heroMenu.activeHero;
					alive = cEngine.playGame(i);
				}
				switch (engine) {
					case "console":
						alive = cEngine.playGame(i);
						break;
					case "gui":
						break;
				}
				i++;
			}
		}
	}
}