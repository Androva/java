package za.wethinkcode.view;

import za.wethinkcode.model.ICharacter;
import za.wethinkcode.model.CharacterFactory;
import za.wethinkcode.model.CustomException;
import za.wethinkcode.model.HeroMenu;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ConsoleChooseHero {
	public Scanner		input;
	private HeroMenu	heroMenu;
	
	public ConsoleChooseHero(HeroMenu heroMenu) {
		this.heroMenu = heroMenu;
	}
	
	public void chooseHero(Scanner input) throws CustomException {
		int i = 1;
		int characterChoice;

		System.out.println("You may choose one of the " + heroMenu.heroes.size() + " heroes below. Choose wisely...");

		for (ICharacter character : heroMenu.heroes) {
			System.out.printf("%d. ", i);
			character.printStats();
			System.out.println();
			i++;
		}

		while (true) {
			try {
				characterChoice = Integer.parseInt(input.next());
				if (characterChoice > heroMenu.heroes.size())
					throw new CustomException("Invalid option. Please try again.");
				else {
					heroMenu.activeHero = heroMenu.heroes.get(characterChoice - 1);
					break;	
				}
			}
			catch (NumberFormatException e) {
				System.out.println("Invalid option. Please try again.");
			}
		}
	}

	public void createHero(Scanner input) throws CustomException {
		String name;
		int cClass;
		String[] classes = { "Fighter", "Tank", "Mage" };

		System.out.println(
				"\n----------------------------------------------------\nChoose a character class:\n1. Fighter\n2. Tank\n3. Mage\n----------------------------------------------------\n");
		while (true) {
			try {
				cClass = Integer.parseInt(input.next());
				if (cClass > 3 || cClass < 1)
					System.out.println("Invalid option. Please try again.");
				else {
					System.out.print("Give your " + classes[cClass - 1] + " a name: ");
					name = input.next();
					name += input.nextLine();
					System.out.println();
					break;
				}
			}
			catch (NumberFormatException e) {
				System.out.println("Invalid option. Please try again.");
			}
		}

		ICharacter hero = CharacterFactory.newCharacter(name.trim(), classes[cClass - 1], null, 1, null);
		heroMenu.activeHero = hero;
		heroMenu.heroes.add(heroMenu.activeHero);

		try {
			FileWriter fileWriter = new FileWriter("./heroes.txt", true);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			printWriter.printf("%s\t%s\t%s\n", hero.getCharacterName(), hero.getCharacterClass(), "1");
			printWriter.close();
		}
		catch (IOException e) {
			System.out.printf("ERROR: %s\n", e.getMessage());
			return;
		}
	}

	public void characterConfirmation() throws CustomException {
		if (heroMenu.activeHero != null) {
			System.out.printf(
					"----------------------------------------------------\nYour character's stats are as follows:\n");
			heroMenu.activeHero.printStats();
			System.out.println("----------------------------------------------------");
		}
		else
			throw new CustomException("There is currently no hero active.");
	}
}