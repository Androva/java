package za.wethinkcode.view;

public class MainMenu {
	public void		displayMainMenu() {
		System.out.println("\n----------------------------------------------------\nWelcome to swingy!\nPlease select an option (1 or 2) below to start.");
		System.out.println("\n1. Choose your hero\n\n\tOR\n\n2. Create a new hero\n\n----------------------------------------------------\n");
	}
}