package za.wethinkcode.view;

import za.wethinkcode.controller.GUIGameEngine;
import za.wethinkcode.model.CharacterFactory;
import za.wethinkcode.model.HeroMenu;
import za.wethinkcode.model.ICharacter;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.ArrayList;
import java.util.List;

public class Gui {
	public JFrame				frame = new JFrame("swingy");
	public JPanel				buttons = new JPanel(new GridBagLayout());
	public GridBagConstraints	c = new GridBagConstraints();
	public JPanel				view = new JPanel(new GridBagLayout());
	static JPanel				heroes = new JPanel(new GridBagLayout());
	static JSplitPane			split1;
	static JSplitPane			fullSplit;
	private HeroMenu			heroMenu;
	public JButton				northButton = new JButton("North");
	public JButton				southButton = new JButton("South");
	public JButton				eastButton = new JButton("East");
	public JButton				westButton = new JButton("West");
	private GUIGameEngine		gameEngine;

	public Gui(HeroMenu heroMenu, GUIGameEngine gameEngine) {
		this.gameEngine = gameEngine;
		this.heroMenu = heroMenu;
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(900, 600));
	}

	private void displayFrame() {
		split1 = new JSplitPane(SwingConstants.HORIZONTAL, true, buttons, heroes);
		fullSplit = new JSplitPane(SwingConstants.VERTICAL, true, split1, view);

		split1.setBorder(null);
		split1.getBottomComponent().setMinimumSize(new Dimension(600, 150));
		split1.getTopComponent().setMinimumSize(new Dimension(300, 150));
		fullSplit.getLeftComponent().setMinimumSize(new Dimension(450, 300));
		fullSplit.getRightComponent().setMinimumSize(new Dimension(450, 300));
		fullSplit.setDividerLocation(frame.getWidth() / 2);

		frame.add(fullSplit);
		frame.setVisible(true);
	}

	public void StartMenu() {
		JButton			createButton = new JButton("Create new Hero");
		JButton			chooseButton = new JButton("Choose a Hero");
		List<JLabel>	availHeroes = new ArrayList<>();

		createButton.addActionListener(new newHero());
		chooseButton.addActionListener(new chooseHero());

		c.fill = GridBagConstraints.VERTICAL;
		c.gridx = 0;
		c.gridy = 0;
		buttons.add(createButton, c);
		c.gridy = 1;
		buttons.add(chooseButton, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		heroes.add(new JLabel("<html><h3><b>Available Heroes:</b></h3></html>"), c);

		int i = 0;
		for (ICharacter hero : heroMenu.heroes) {
			availHeroes.add(new JLabel());
			availHeroes.get(i)
					.setText("<html>" + availHeroes.get(i).getText() + "<b>" + hero.getStats("name") + "</b>"
							+ "<br/>Class: " + hero.getStats("class") + "<br/>Attack: " + hero.getStats("ad")
							+ "<br/>Defence: " + hero.getStats("def") + "<br/>HP: " + hero.getStats("hp"));
			c.gridx = i;
			c.gridy = 1;
			c.insets = new Insets(0, 10, 0, 10);
			heroes.add(availHeroes.get(i), c);
			i++;
		}
		displayFrame();
	}

	public class startGame implements ActionListener {
		JLabel	heroStats;
		public void actionPerformed (ActionEvent e) {
			heroStats = new JLabel("<html><b>" + heroMenu.activeHero.getCharacterName() + "'s stats: </b><br/>");
			heroStats.setText(heroStats.getText() + "Class:\t[" + heroMenu.activeHero.getCharacterClass() + "]<br/>");
			heroStats.setText(heroStats.getText() + "Lvl:\t[" + heroMenu.activeHero.getLevel() + "]<br/>");
			heroStats.setText(heroStats.getText() + "HP:\t[" + heroMenu.activeHero.getHp() + "]<br/>");
			heroStats.setText(heroStats.getText() + "XP:\t[" + heroMenu.activeHero.getXp() + "]<br/>");
			heroStats.setText(heroStats.getText() + "Dmg:\t[" + heroMenu.activeHero.getAd() + "]<br/>");
			heroStats.setText(heroStats.getText() + "Def:\t[" + heroMenu.activeHero.getDef() + "]<br/></html>");

			buttons.removeAll();
			heroes.removeAll();
			buttons.add(heroStats);
			buttons.revalidate();
			buttons.repaint();
			heroes.revalidate();
			heroes.repaint();
			gameEngine.playMap();
		}
	}

	public class createHero implements ActionListener {
		JLabel		heroStats;
		ButtonGroup	heroClass;
		@NotNull (message = "You must choose a class for your hero")
		String		heroClassChoice;
		JTextField	heroName;
		@NotNull (message = "Name cannot be null")
		@Size (min = 3, max = 20, message = "Hero name must be between 3 and 20 character's long")
		String		heroNameChoice;
		JButton		start = new JButton("Begin");

		public	createHero(ButtonGroup heroClass, JTextField heroName) {
			this.heroClass = heroClass;
			this.heroName = heroName;
		}
	
		public void actionPerformed(ActionEvent e) {
			try {
				heroClassChoice = heroClass.getSelection().getActionCommand();
				heroNameChoice = heroName.getText();

				ICharacter newHero = CharacterFactory.newCharacter(heroNameChoice, heroClassChoice, null, 1, null);
				heroMenu.activeHero = newHero;
				heroMenu.heroes.add(newHero);

				heroStats = new JLabel("<html><b>" + heroMenu.activeHero.getCharacterName() + "'s stats: </b><br/>");
				heroStats.setText(heroStats.getText() + "Class:\t[" + heroMenu.activeHero.getCharacterClass() + "]<br/>");
				heroStats.setText(heroStats.getText() + "Lvl:\t[" + heroMenu.activeHero.getLevel() + "]<br/>");
				heroStats.setText(heroStats.getText() + "HP:\t[" + heroMenu.activeHero.getHp() + "]<br/>");
				heroStats.setText(heroStats.getText() + "XP:\t[" + heroMenu.activeHero.getXp() + "]<br/>");
				heroStats.setText(heroStats.getText() + "Dmg:\t[" + heroMenu.activeHero.getAd() + "]<br/>");
				heroStats.setText(heroStats.getText() + "Def:\t[" + heroMenu.activeHero.getDef() + "]<br/></html>");

				heroMenu.activeHero.printStats();
				heroes.removeAll();
				
				boolean	exist = false;
				Component[] buttonsComp = buttons.getComponents();
				for (Component c : buttonsComp) {
					if (c.getParent() == buttons && c.getName() == "Start") {
						exist = true;
						break;
					}
				}
				if (exist == false) {
					start.setName("Start");
					buttons.add(start);
					start.addActionListener(new startGame());
				}
				buttons.revalidate();
				buttons.repaint();

				heroes.add(heroStats);
				heroes.revalidate();
				heroes.repaint();
			}
			catch (NullPointerException error) {
				System.out.println("Please select a valid class.");
			}
		}
	}

	public class setHero implements ActionListener {
		JLabel	heroStats;
		JButton start = new JButton("Begin");
		JButton chooseHero = new JButton("Choose a Hero");
		JButton createHero = new JButton("Create a Hero");

		public void actionPerformed(ActionEvent e) {
			System.out.println(e.getActionCommand());

			for (ICharacter hero : heroMenu.heroes) {
				if (hero.getCharacterName() == e.getActionCommand())
					heroMenu.activeHero = hero;
			}

			heroStats = new JLabel("<html><b>" + heroMenu.activeHero.getCharacterName() + "'s stats: </b><br/>");
			heroStats.setText(heroStats.getText() + "Class:\t[" + heroMenu.activeHero.getCharacterClass() + "]<br/>");
			heroStats.setText(heroStats.getText() + "Lvl:\t[" + heroMenu.activeHero.getLevel() + "]<br/>");
			heroStats.setText(heroStats.getText() + "HP:\t[" + heroMenu.activeHero.getHp() + "]<br/>");
			heroStats.setText(heroStats.getText() + "XP:\t[" + heroMenu.activeHero.getXp() + "]<br/>");
			heroStats.setText(heroStats.getText() + "Dmg:\t[" + heroMenu.activeHero.getAd() + "]<br/>");
			heroStats.setText(heroStats.getText() + "Def:\t[" + heroMenu.activeHero.getDef() + "]<br/></html>");

			heroMenu.activeHero.printStats();
			heroes.removeAll();
			heroes.add(heroStats);
			heroes.revalidate();
			heroes.repaint();
			
			start.addActionListener(new startGame());
			start.setName("Start");
			chooseHero.addActionListener(new chooseHero());
			createHero.addActionListener(new newHero());

			buttons.removeAll();
			buttons.add(start);
			buttons.add(chooseHero);
			buttons.add(createHero);
			buttons.revalidate();
			buttons.repaint();
		}
	}

	public class chooseHero implements ActionListener {
		List<JButton> heroButtons = new ArrayList<JButton>();

		public void actionPerformed(ActionEvent e) {
			int i = 0;
			JButton current;

			buttons.removeAll();
			for (ICharacter hero : heroMenu.heroes) {
				heroButtons.add(new JButton(hero.getCharacterName()));
				current = heroButtons.get(i);
				current.setActionCommand(hero.getCharacterName());
				current.addActionListener(new setHero());
				buttons.add(current);
				i++;
			}

			buttons.revalidate();
			buttons.repaint();
		}
	}

	public class newHero implements ActionListener {
		JRadioButton	classMage = new JRadioButton("Mage");
		JRadioButton	classFighter = new JRadioButton("Fighter");
		JRadioButton	classTank = new JRadioButton("Tank");
		ButtonGroup		buttons = new ButtonGroup();
		JButton			submit = new JButton("Create Hero");
		JLabel			description = new JLabel("<html> Choose a class: </html>");
		JLabel			nameLabel = new JLabel("<html> Give your Hero a name: </html>");
		JTextField		heroName = new JTextField(20);
		JLabel			errors = new JLabel();

		public void actionPerformed(ActionEvent e) {
			heroes.removeAll();

			classFighter.setActionCommand(classFighter.getText());
			classMage.setActionCommand(classMage.getText());
			classTank.setActionCommand(classTank.getText());
			buttons.add(classFighter);
			buttons.add(classMage);
			buttons.add(classTank);

			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 0;
			c.gridwidth = 3;
			heroes.add(description, c);
			c.gridx = 0;
			c.gridy = 1;
			c.gridwidth = 1;
			heroes.add(classMage, c);
			c.gridx = 1;
			heroes.add(classFighter, c);
			c.gridx = 2;
			heroes.add(classTank, c);
			c.gridx = 0;
			c.gridy = 2;
			c.gridwidth = 2;
			heroes.add(nameLabel, c);
			c.gridx = 2;
			c.gridwidth = 1;
			heroes.add(heroName, c);
			c.gridx = 0;
			c.gridy = 3;
			c.gridwidth = 3;
			submit.addActionListener(new createHero(buttons, heroName));
			heroes.add(submit, c);

			heroes.revalidate();
			heroes.repaint();
		}
	}
}